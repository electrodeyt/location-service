Source: location-service
Section: utils
Priority: optional
Maintainer: UBports developers <devs@ubports.com>
Build-Depends: cmake,
               curl,
               libdbus-cpp-dev (>= 4.1.0),
               dh-exec,
               debhelper-compat (= 12),
               doxygen <!nodoc>,
               google-mock (>= 1.6.0+svn437),
               graphviz <!nodoc>,
               libapparmor-dev,
               libboost-filesystem-dev,
               libboost-program-options-dev,
               libboost-system-dev,
               libdbus-1-dev,
               libdbus-cpp-dev,
               libgoogle-glog-dev,
               libgtest-dev,
               libiw-dev,
               libjson-c-dev,
               libnet-cpp-dev,
               libprocess-cpp-dev,
               libtrust-store-dev,
               libubuntu-platform-hardware-api-headers [!ppc64el !powerpc] <!noplatformapi>,
               libubuntu-platform-hardware-api-dev [!ppc64el !powerpc] <!noplatformapi>,
               libproperties-cpp-dev,
               lsb-release,
               qtbase5-dev,
               qtlocation5-dev,
               qtpositioning5-dev,
               trust-store-bin,
               cmake-extras,
               libgps-dev
Standards-Version: 3.9.4
Homepage: https://gitlab.com/ubports/development/core/location-service
Vcs-Git: https://gitlab.com/ubports/development/core/location-service.git
Vcs-Browser: https://gitlab.com/ubports/development/core/location-service

Package: liblomiri-location-service3
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains the shared library needed by client applications.

Package: liblomiri-location-service-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Recommends: lomiri-location-service-doc,
Depends: liblomiri-location-service3 (= ${binary:Version}),
         libdbus-1-dev,
         libdbus-cpp-dev,
         libboost-dev,
         ${misc:Depends},
Suggests: lomiri-location-service-doc,
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains header files required to develop clients talking to the Lomiri
 location service.

Package: lomiri-location-service-tests
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains all test executables.

Package: lomiri-location-service-bin
Architecture: any
Depends: liblomiri-location-service3 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         trust-store-bin,
Breaks: lomiri-location-service-examples (<< 0.0.2),
Replaces: lomiri-location-service-examples (<< 0.0.2),
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains the service executable and man pages.

Package: lomiri-location-service-examples
Architecture: any
Multi-Arch: same
Depends: liblomiri-location-service3 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         lomiri-location-service-doc,
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains example service and client.

Package: lomiri-location-service-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Build-Profiles: <!nodoc>
Recommends: lomiri-location-service-examples,
Description: location service aggregating position/velocity/heading
 Aggregates position/velocity/heading updates and exports them over dbus.
 .
 Contains documentation for service and client.
